function getUselesss() {
  fetch("http://192.168.2.17:8080/admin/uselesss", {
      headers: addAuthorizationHeader({})
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => response.json())
    .then(uselesss => displayUselesss(uselesss));

}

function displayUselesss(uselesss) {
  // document.getElementById('uselessLink').href = uselesss.url;
  let uselessList = document.getElementById('uselessList');
  uselessList.innerHTML = "";

  for (let i = 0; i < uselesss.length; i++) {
    let uselessRow = "";
    uselessRow = "<tr>";
    uselessRow = uselessRow + "<td>" + uselesss[i].id + "</td>";
    uselessRow = uselessRow + "<td>" + uselesss[i].url + "</td>";
    uselessRow = uselessRow + "<td>";
    uselessRow = uselessRow + '<div class="homepageButtons"><button type="button" class="btn btn-danger" onClick="deleteUseless(' + uselesss[i].id + ')">Delete</button></div>' +
      '<div class="homepageButtons"><button type="button" class="btn btn-dark" onClick="openUselessModal(' + uselesss[i].id + ')">Edit</button></div>';
    uselessRow = uselessRow + "</tr>";
    uselessList.innerHTML += uselessRow;
  }
}

function addUseless() {
  const url = document.getElementById('uUrl').value;
  const addUrl = 'http://localhost:8080/admin/useless';
  fetch(addUrl, {
      method: "POST",
      headers: addAuthorizationHeader({
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({

      }),
      body: JSON.stringify({
        'url': url

      })
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      getUselesss();
      closeUselessModal();
    });

}


function editUseless() {
  const id = document.getElementById('uId').value;
  const url = document.getElementById('uUrl').value;

  const editUrl = 'http://localhost:8080/admin/useless';
  fetch(editUrl, {
      method: "PUT",
      headers: addAuthorizationHeader({
        "Content-Type": "application/json"
      }),

      body: JSON.stringify({
        'id': id,
        'url': url,
      })
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      getUselesss();
      closeUselessModal();
    });

}

function addOrEditUseless() {
  const id = document.getElementById("uId").value;
  if (id > 0) {
    editUseless();
  } else {
    addUseless();
  }
}

function openUselessModal(id) {

  document.getElementById('uId').value = null;
  document.getElementById('uUrl').value = null;
  $('#uselessModal').modal('show');

  if (id > 0) {

    const getUselessUrl = 'http://localhost:8080/admin/useless/' + id;
    fetch(getUselessUrl, {
        headers: addAuthorizationHeader({})
      })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))
      .then(response => response.json())
      .then(useless => {


        document.getElementById('uId').value = useless.id;
        document.getElementById('uUrl').value = useless.url;

      });

  }

}

function closeUselessModal() {
  $('#uselessModal').modal('hide');


}


function deleteUseless(id) {
  if (confirm('Are you sure?')) {
    const deleteurl = 'http://localhost:8080/admin/useless/' + id;
    fetch(deleteurl, {
        method: "DELETE",
        headers: addAuthorizationHeader({})
      })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))

      .then(response => getUselesss());

  }

}


function getLifehacks() {
  fetch("http://192.168.2.17:8080/admin/lifehacks", {
      headers: addAuthorizationHeader({})
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => response.json())
    .then(lifehacks => displayLifehacks(lifehacks));
}


function displayLifehacks(lifehacks) {
  let lifehackList = document.getElementById('lifehackList');
  lifehackList.innerHTML = "";

  for (let i = 0; i < lifehacks.length; i++) {
    let lifehackRow = "";
    lifehackRow = "<tr>";
    // videoRow = videoRow + '<iframe width="560" height="315" src="https://www.youtube.com/embed/NyDC48eglYo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
    lifehackRow = lifehackRow + "<td>" + lifehacks[i].id + "</td>";
    lifehackRow = lifehackRow + "<td>" + lifehacks[i].url + "</td>";
    lifehackRow = lifehackRow + "<td>";
    lifehackRow = lifehackRow + '<div class="homepageButtons"><button type="button" class="btn btn-danger" onClick="deleteLifehack(' + lifehacks[i].id + ')">Delete</button></div>' +
      '<div class="homepageButtons"><button type="button" class="btn btn-dark" onClick="openLifehackModal(' + lifehacks[i].id + ')">Edit</button></div>';
    lifehackRow = lifehackRow + "</div></td>";
    lifehackRow = lifehackRow + "</tr>";
    lifehackList.innerHTML += lifehackRow;

  }
}

function addLifehack() {
  const url = document.getElementById('hUrl').value;
  const addUrl = 'http://localhost:8080/admin/lifehack';
  fetch(addUrl, {
      method: "POST",
      headers: addAuthorizationHeader({
        "Content-Type": "application/json"
      }),

      body: JSON.stringify({
        'url': url

      })
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      getLifehacks();
      closeLifehackModal();
    });

}


function editLifehack() {
  const id = document.getElementById('hackId').value;
  const url = document.getElementById('hUrl').value;

  const editUrl = 'http://localhost:8080/admin/lifehack';
  fetch(editUrl, {
      method: "PUT",
      headers: addAuthorizationHeader({
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({
        'id': id,
        'url': url
      })
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      getLifehacks();
      closeLifehackModal();
    });

}

function addOrEditLifehack() {
  const id = document.getElementById('hackId').value;
  if (id > 0) {
    editLifehack();
  } else {
    addLifehack();
  }

}

function openLifehackModal(id) {

  document.getElementById('hackId').value = null;
  document.getElementById('hUrl').value = null;
  $('#lifehackModal').modal('show');

  if (id > 0) {

    const getLifehackUrl = 'http://localhost:8080/admin/lifehack/' + id;
    fetch(getLifehackUrl, {
        headers: addAuthorizationHeader({})
      })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))
      .then(response => response.json())
      .then(lifehack => {


        document.getElementById('hackId').value = lifehack.id;
        document.getElementById('hUrl').value = lifehack.url;

      });

  }

}

function closeLifehackModal() {
  $('#lifehackModal').modal('hide');


}


function deleteLifehack(id) {
  if (confirm('Are you sure?')) {
    const deleteurl = 'http://localhost:8080/admin/lifehack/' + id;
    fetch(deleteurl, {
        method: "DELETE",
        headers: addAuthorizationHeader({})
      })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))

      .then(response => getLifehacks());

  }

}



function getVideos() {
  fetch("http://192.168.2.17:8080/admin/videos", {
      headers: addAuthorizationHeader({})
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => response.json())
    .then(videos => displayVideos(videos));
}


function displayVideos(videos) {
  let videoList = document.getElementById('videoList');
  videoList.innerHTML = "";

  for (let i = 0; i < videos.length; i++) {
    let videoRow = "";
    videoRow = "<tr>";
    videoRow = videoRow + "<td>" + videos[i].id + "</td>";
    videoRow = videoRow + "<td>" + videos[i].url;
    videoRow = videoRow + "<td>";
    videoRow = videoRow + '<div class="homepageButtons"><button type="button" class="btn btn-danger" onClick="deleteVideo(' + videos[i].id + ')">Delete</button></div>' +
      '<div class="homepageButtons"><button type="button" class="btn btn-dark" onClick="openVideoModal(' + videos[i].id + ')">Edit</button></div>';
    videoRow = videoRow + "</div></td>";
    videoRow = videoRow + "</tr>";
    videoList.innerHTML += videoRow;

  }
}

function addVideo() {
  const url = document.getElementById('vUrl').value;
  const addUrl = 'http://localhost:8080/admin/video';
  fetch(addUrl, {
      method: "POST",
      headers: addAuthorizationHeader({
        "Content-Type": "application/json"
      }),

      body: JSON.stringify({
        'url': url

      })
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      getVideos();
      closeVideoModal();
    });

}

function editVideo() {
  const id = document.getElementById('vId').value;
  const url = document.getElementById('vUrl').value;

  const editUrl = 'http://localhost:8080/admin/video';
  fetch(editUrl, {
      method: "PUT",
      headers: addAuthorizationHeader({
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({
        'id': id,
        'url': url
      })
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      getVideos();
      closeVideoModal();
    });

}

function addOrEditVideo() {
  const id = document.getElementById('vId').value;
  if (id > 0) {
    editVideo();
  } else {
    addVideo();
  }

}

function openVideoModal(id) {

  document.getElementById('vId').value = null;
  document.getElementById('vUrl').value = null;
  $('#videoModal').modal('show');

  if (id > 0) {

    const getVideoUrl = 'http://localhost:8080/admin/video/' + id;
    fetch(getVideoUrl, {
        headers: addAuthorizationHeader({})
      })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))
      .then(response => response.json())
      .then(video => {


        document.getElementById('vId').value = video.id;
        document.getElementById('vUrl').value = video.url;

      });

  }

}

function closeVideoModal() {
  $('#videoModal').modal('hide');


}

function deleteVideo(id) {
  if (confirm('Are u sure?')) {
    const deleteurl = "http://localhost:8080/admin/video/" +
      id;
    fetch(deleteurl, {
        method: "DELETE",
        headers: addAuthorizationHeader({})
      })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))
      .then(response => getVideos());

  }

}

function getQuotes() {
  fetch("http://192.168.2.17:8080/admin/quotes", {
      headers: addAuthorizationHeader({})
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => response.json())
    .then(quotes => displayQuotes(quotes));
}

function displayQuotes(quotes) {
  let quoteList = document.getElementById('quoteList');
  quoteList.innerHTML = "";

  for (let i = 0; i < quotes.length; i++) {
    let quoteRow = "";
    quoteRow = "<tr>";
    quoteRow = quoteRow + "<td>" + quotes[i].id + "</td>";
    quoteRow = quoteRow + "<td>" + quotes[i].body + "</td>";
    quoteRow = quoteRow + "<td>" + quotes[i].author + "</td>";
    quoteRow = quoteRow + "<td>";
    quoteRow = quoteRow + '<div class="homepageButtons"><button type="button" class="btn btn-danger" onClick="deleteQuote(' + quotes[i].id + ')">Delete</button></div>' +
      '<div class="homepageButtons"><button type="button" class="btn btn-dark" onClick="openQuoteModal(' + quotes[i].id + ')">Edit</button></div>';
    quoteRow = quoteRow + "</div></td>";
    quoteRow = quoteRow + "</tr>";
    quoteList.innerHTML += quoteRow;

  }

}

function addQuote() {
  const body = document.getElementById('qBody').value;
  const author = document.getElementById('qAuthor').value;
  const addUrl = "http://localhost:8080/admin/quote";
  fetch(addUrl, {
      method: "POST",
      headers: addAuthorizationHeader({
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({
        'body': body,
        'author': author

      })
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      getQuotes();
      closeQuoteModal();
    });

}

function editQuote() {
  const id = document.getElementById('qId').value;
  const body = document.getElementById('qBody').value;
  const author = document.getElementById('qAuthor').value;

  const editUrl = "http://localhost:8080/admin/quote";
  fetch(editUrl, {
      method: "PUT",
      headers: addAuthorizationHeader({
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({
        'id': id,
        'body': body,
        'author': author

      })
    })
    .then(response => clearTokenAndRedirectIfUnauthorized(response))
    .then(response => {
      getQuotes();
      closeQuoteModal();
    });

}

function addOrEditQuote() {
  const id = document.getElementById('qId').value;
  if (id > 0) {
    editQuote();
  } else {
    addQuote();
  }

}

function openQuoteModal(id) {

  document.getElementById('qId').value = null;
  document.getElementById('qBody').value = null;
  document.getElementById('qAuthor').value = null;
  $('#quoteModal').modal('show');

  if (id > 0) {

    const getQuoteUrl = "http://localhost:8080/admin/quote/" +
      id;
    fetch(getQuoteUrl, {
        headers: addAuthorizationHeader({})
      })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))
      .then(response => response.json())
      .then(quote => {


        document.getElementById('qId').value = quote.id;
        document.getElementById('qBody').value = quote.body;
        document.getElementById('qAuthor').value = quote.author;

      });

  }

}

function closeQuoteModal() {
  $('#quoteModal').modal('hide');


}

function deleteQuote(id) {
  if (confirm('Are u sure?')) {
    const deleteurl = "http://localhost:8080/admin/quote/" +
      id;
    fetch(deleteurl, {
        method: "DELETE",
        headers: addAuthorizationHeader({})
      })
      .then(response => clearTokenAndRedirectIfUnauthorized(response))
      .then(response => getQuotes());

  }
}