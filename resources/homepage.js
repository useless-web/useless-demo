function getQuotes() {
  fetch('http://192.168.2.17:8080/quotes')
    .then(response => response.json())
    .then(quotes => displayQuotes(quotes));
}

function displayQuotes(quotes) {
  let quoteList = document.getElementById('quoteList');
  quoteList.innerHTML = "";

  for (let i = 0; i < quotes.length; i++) {
    let quoteRow = "";
    quoteRow = "<tr>";
    quoteRow = quotes[i].body + " ";
    quoteRow = quoteRow + quotes[i].author + "</td>";
    quoteRow = quoteRow + "</tr>";
    quoteList.innerHTML += quoteRow;

  }

}

function getVideos() {
  fetch('http://192.168.2.17:8080/videos')
    .then(response => response.json())
    .then(videos => displayVideos(videos));
}

function displayVideos(videos) {
  let videoList = document.getElementById('videoList');
  videoList.innerHTML = "";

  for (let i = 0; i < videos.length; i++) {
    let videoRow = "";
    // videoRow = "<span>";
    // videoRow = videoRow + '<iframe width="560" height="315" src="https://www.youtube.com/embed/NyDC48eglYo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
    videoRow = videos[i].url;
    // videoRow = videoRow + "</span>";
    videoList.innerHTML += videoRow;

  }
}

function getLifehacks() {
  fetch('http://192.168.2.17:8080/lifehacks')
    .then(response => response.json())
    .then(lifehacks => displayLifehacks(lifehacks));
}

function displayLifehacks(lifehacks) {
  let lifehackList = document.getElementById('lifehackList');
  lifehackList.innerHTML = "";

  for (let i = 0; i < lifehacks.length; i++) {
    let lifehackRow = "";
    lifehackRow = "<span>";
    // videoRow = videoRow + '<iframe width="560" height="315" src="https://www.youtube.com/embed/NyDC48eglYo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
    lifehackRow = lifehacks[i].url;
    lifehackRow = lifehackRow + "</span>";
    lifehackList.innerHTML += lifehackRow;

  }
}

function getUselesss() {
  fetch('http://192.168.2.17:8080/uselesss')
    .then(response => response.json())
    .then(uselesss => displayUselesss(uselesss));
}

function displayUselesss(uselesss) {
  document.getElementById('uselessLink').href = uselesss[0].url;
  // let uselessList = document.getElementById('uselessList');
  // uselessList.innerHTML = "";
  //
  // for (let i = 0; i < uselesss.length; i++) {
  //   let uselessRow = "";
  //   uselessRow = "<span>";
  //   uselessRow = uselesss[i].url;
  //   uselessRow = uselessRow + "</span>";
  //   uselessRow.innerHTML += uselessRow;
  // }
}